# == Class: pupker
#
# Puppet/Docker setup for HelloScalatra exercise
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { 'pupker':
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <bluebobx@gmail.com>
#
# === Copyright
#
# Copyright 2016 Your name here, unless otherwise noted.
#
class bluebobx-pupker {
  include 'aws'
  include 'docker'

  ec2_securitygroup { 'hsdocktest' :
    ensure => present,
    region => 'eu-west-1',
    description => 'hello scalatra stuff',
    ingress => [ 
      { 
        protocol => 'tcp',
        port => 22,
        cidr => '0.0.0.0/0',
      },
      {
        protocol => 'tcp',
        port => 8080,
        cidr => '0.0.0.0/0',
      }
    ]
  }

  ec2_instance { 'dokrhs' :
    ensure => present,
    region => 'eu-west-1',
    image_id => 'ami-f95ef58a',
    instance_type => 't2.micro',
    key_name => 'qwikLABS-L489-23878',
    security_groups => [ 'hsdocktest' ],
  }

  package { 'docker' :
    ensure => present,
  }

  docker::image { 'bluebobx/hello-scalatra' :
    ensure => present,
  }

  docker::run { 'hello-scalatra' :
    image => 'bluebobx/hello-scalatra',
    ports => [ '8080', '8080' ],
  }

}
